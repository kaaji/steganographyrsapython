import multiprocessing as mp
import os
import tkinter as tk
from datetime import datetime
from tkinter.filedialog import askopenfilename
from tkinter import messagebox as mb

from PIL import Image

from constants import mapping_dict_enc, e, n, d, number, letter
from img_processing import encode, decode
import histo as h

window = tk.Tk()
IMG_FRMTS = [("JPEG", "*.jpg"), ("JPEG", "*.jpeg"), ("PNG", "*.png")]
decrypt_flag = True
encoded_image_file = None

mapping_dict_dec = dict(
    map(reversed, mapping_dict_enc.items())
)
def cipher(num, e, X):
    # global X
    for i in range(len(num)):
        X.append((int(num[i]) ** e) % n)

def decipher(num,d,Y):
    for i in range(len(num)):
        Y.append((int(num[i])**d)%n)

def encrypt(plaintext, original_image_file):
    global encoded_image_file
    _root_dir = os.getcwd()
    _encrypts_dir =os.path.join(_root_dir, 'encrypts')
    # encrypts a plaintext message using the current key
    # global numC, X
    X = []
    # plaintext = (input("Enter Plaintext :"))
    # plaintext = (plaintext.lower())
    numC = []
    for _letter in plaintext:
        numC.append(mapping_dict_enc[_letter])
    cipher(numC, e, X)
    print("Ciphertext:", X)
    print("Number of Ciphertext blocks:", len(X))
    cipher_block.config(text=f"Ciphertext:{X}")
    number_of_cipher_blocks.config(text=f"Number of Ciphertext block:{len(X)}")
    cipher_block.pack()
    number_of_cipher_blocks.pack()
    img = Image.open(original_image_file)
    print(img, img.mode)
    if not os.path.exists(_encrypts_dir):
        os.mkdir(_encrypts_dir)
    today =datetime.now().date()
    today_enc_folder = os.path.join(_encrypts_dir, str(today))
    if not os.path.exists(today_enc_folder):
        os.mkdir(today_enc_folder)
    encoded_image_file_path = os.path.join(today_enc_folder, f"enc_{original_image_file.split('/')[-1]}")
    img_encoded = encode(img, plaintext, X)
    if img_encoded:
        img_encoded.save(encoded_image_file_path)
        # print("{} saved!".format(encoded_image_file_path))
        mb.showinfo(title='ENCODED! YAYYYY!', message=f"{encoded_image_file_path} saved!")
    encoded_image_file = encoded_image_file_path
    p1 = mp.Process(target=h.draw, args=(original_image_file,))
    p2 = mp.Process(target=h.draw, args=(encoded_image_file,))
    p1.start()
    p2.start()

def decrypt():
    global i,j, encoded_image_file
    Y=[]
    img2 = Image.open(encoded_image_file)
    print(img2, img2.mode)
    hidden_text = decode(img2)
    print(hidden_text)
    decipher(hidden_text,d, Y)
    numD=[]
    numDtest=[]
    for i in Y:
        numDtest.append(
            mapping_dict_dec.get(str(i))
        )
    for i in range(len(Y)):
        for j in range(len(number)):
            if(Y[i]==int(number[j])):
                numD.append(letter[j])
    final_msg = ''.join(map(str,numD))
    mb._show(title='Encrypted message', message=final_msg)

def choosepic():
    global e1
    path_ = askopenfilename(filetypes=IMG_FRMTS, title='Please select a picture')
    path.set(path_)


def _process_args():
    _msg_txt, _img_path = msg_text.get().lower(), path.get()
    if not any([_msg_txt,_img_path]):
        msg = ''
        print(_msg_txt)
        if not _msg_txt:
            msg = 'No message was given.'
        if not _img_path:
            msg = 'Invalid image.'
        mb.showerror("Oh no!", msg)
    encrypt(_msg_txt, _img_path)
    button.config(text='Decrypt', command=decrypt, bg="red",)
    entry.config(state='readonly')


path = tk.StringVar()
msg_text = tk.StringVar()

window.geometry("400x250")
greeting = tk.Label(text="Hide & Seek")
message = tk.Label(text='Enter message:')
entry = tk.Entry(fg="black", bg="white", width=50, textvariable=msg_text)
greeting.pack()
message.pack()
entry.pack()

tk.Button(window, text='select picture', command=choosepic).pack()
e1 = tk.Entry(window, state='readonly', text=path)
e1.pack()
button = tk.Button(
    text="Encrypt",
    width=6,
    height=2,
    bg="blue",
    fg="yellow",
    command=_process_args
)
button.pack()
cipher_block = tk.Label()
number_of_cipher_blocks = tk.Label()
window.mainloop()
