'''RSA ALGO constants start here'''
n = 2537
e = 13
d = 937
'''RSA ALGO constants end here'''
#########################################################
'''Other constants start here'''
RGB = 'RGB'
TEXT_LIMIT = 255
letter = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v",
          "w", "x", "y", "z", ",", ".", "!", "?", " "]
number = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18",
          "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"]

mapping_dict_enc = dict(
    zip(letter, number)
)
