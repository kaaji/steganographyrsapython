from PIL import Image
from matplotlib import pyplot as plt


def hexencode(rgb):
    # converts (r,g,b) tuple to Hex Triplet value
    r = rgb[0]
    g = rgb[1]
    b = rgb[2]
    return '#%02x%02x%02x' % (r, g, b)


def draw(name):
    # function to draw histogram
    im = Image.open(name)
    w, h = im.size
    colors = im.getcolors(w * h)
    limited_colors = colors[0:600]
    for idx, c in enumerate(limited_colors):
        plt.bar(idx, c[0], color=hexencode(c[1]))
    plt.show()
